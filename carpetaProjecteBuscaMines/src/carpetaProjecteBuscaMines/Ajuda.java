package carpetaProjecteBuscaMines;

/**
 * Classe on gestionem el suport a l'usuari.
 * @version 1.0
 * @author arnauicat
 * @since 14-02-2022
 */

public class Ajuda {
	
	/**
	 * M�tode que imprimeix l'ajuda per a l'usuari si el criden.
	 * */
	//hola
	public static void mostrarInfo() {
		
		System.out.println(" -----------------------------Ajuda------------------------------");
		System.out.println("|� Per jugar primer has de seleccionar les opcions		�|");
		System.out.println("|� Guanyes quan has destapat totes les caselles que no son mines�|");
		System.out.println("|� Perdr�s quan destapis una mina				�|");
		System.out.println("|� Pots consultar el r�nquing seleccionat el numero	     �|");
		System.out.println("|� Per sortir haur�s d'apuntar el numero 100 al xat            �|");
		System.out.println(" ----------------------------------------------------------------");
		System.out.println();
		
	}
	
}