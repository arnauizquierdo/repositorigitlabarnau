package carpetaProjecteBuscaMines;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

/**
 * Classe encarregada de gestionar el ranking de jugadors amb els seus
 * punts i demanar el nom d'usuari.
 * @version 1.0
 * @author arnauicat
 * @since 14-02-2022
 */

public class Visualitza {
	
	static Scanner reader = new Scanner(System.in);
	static ArrayList<String> name = new ArrayList<String>();  //guarda els name dels jugadors
	static ArrayList<Integer> contPartidesGuanyades = new ArrayList<Integer>();  //guarda el nombre de partides guanyades per cada jugador
	static ArrayList<Integer> contPartidesPerdudes = new ArrayList<Integer>(); //guarda el nombre de partides perdudes per cada jugador
	
	/**
	 * Metode per imprimir el ranking de jugadors. 
	 * */
	
	public static void mostrarJugador() {
		
		if (name.size()==0) {
			System.out.println(" --> No s'ha jugat cap partida <--\n\n");
		}
		
		else {
			System.out.println("----------------------------");
			for (int i = 0; i < name.size(); i++) {
				String jugador = name.get(i);
				System.out.println("Resultats de "+jugador);
				System.out.println("Partides guanyades:  " + contPartidesGuanyades.get(i));
				System.out.println("Partides perdudes:  " + contPartidesPerdudes.get(i));
			}
			System.out.println("----------------------------\n");
		}
		
	}
	
	/**
	 * M�tode que demana el nom d'un jugador i despr�s el guarda al r�nquing.
	 * @return Retorna el el nom que li hem donat.
	 * */
	
	
	public static String definirJugador() {
		String nom;
		
		System.out.print("Indica el nom del jugador: ");
		nom = reader.nextLine().toUpperCase();
		
		if (!name.contains(nom)) {
			name.add(nom);
			contPartidesGuanyades.add(0);
			contPartidesPerdudes.add(0);
		}
		System.out.println();
		return nom;
	}

	/**
	 * M�tode per actualitzar els punts del jugador, tant si ha guanyat o perdut.
	 * @param guanya el False correspon al marcador de guanyades i True al de perdudes.
	 * @param jugador fa referencia al jugador al qual li estem actualitzant el marcador.
	 * */
	
	public static void actualitzarJugador(boolean guanya, String jugador) {
		
		if (name.contains(jugador)) {
			int pos = name.indexOf(jugador);
		
			if (!guanya) contPartidesGuanyades.set(pos, contPartidesGuanyades.get(pos) + 1);
			else contPartidesPerdudes.set(pos,  contPartidesPerdudes.get(pos) + 1);
		}
	}
}