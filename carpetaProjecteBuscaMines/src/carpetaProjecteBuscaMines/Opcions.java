package carpetaProjecteBuscaMines;
import java.util.Random;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Classe dedicada a la gestio de les dimensions del taulell i al nom de l'usuari.
 * @version 1.0
 * @author arnauicat
 * @since 14-02-2022
 * */


public class Opcions {
	
	/**
	 * Declaraci�  de l'entrada per teclat.
	 * */

	static Scanner reader = new Scanner(System.in);
	
	/**
	 * Metode on es demana que escollim entre tres opcions, en relaci� a les dimensions del taulell i les mines.
	 * @return retornem una ArrayList amb les dimensions de: files, columnes i quantitat de mines. 
	 * */
	
	public static ArrayList escollirOpcions() {
		//retorna les 2 dimensions del taulell i el numero de mines
		int opcions = 0;
		do {
			System.out.print("(1) 8  x 8  --> 10 mines\n");
			System.out.print("(2) 16 x 16 --> 40 mines\n");
			System.out.print("(3) 16 x 30 --> 99 mines\n");
			System.out.print("Escriu el numero de l'opcio: ");
			opcions = reader.nextInt();
		} while (opcions < 1 || opcions > 3);
		
		System.out.println();
		
		ArrayList<Integer> opcionsArrayList = new ArrayList<Integer>();
				
		if (opcions==1) {
			opcionsArrayList.add(8);
			opcionsArrayList.add(8);
			opcionsArrayList.add(10);
		}
		else if (opcions==2) {
			opcionsArrayList.add(16);
			opcionsArrayList.add(16);
			opcionsArrayList.add(40);
		}
		else {
			opcionsArrayList.add(16);
			opcionsArrayList.add(30);
			opcionsArrayList.add(99);
		}
		
		return opcionsArrayList;
	}
	
	/**
	 * Metode on es centralitza el funcionament per demanar les dimensions del taulell i el nom del jugador
	 * @return retorna una ArrayList amb: (nomJugador(files, columnes, mines))
	 * */
	
	public static ArrayList opcions() {
		//com a index 0 retorna el nom del jugador, com a index 1 retorna les opcions
		ArrayList opcions = new ArrayList();
		
		String nom = Visualitza.definirJugador();
		
		opcions.add(nom); // nom jugador
		
		ArrayList escollirOpcions = escollirOpcions();
		
		opcions.add(escollirOpcions); //opcions escollides
		
		return opcions;

	}	

}