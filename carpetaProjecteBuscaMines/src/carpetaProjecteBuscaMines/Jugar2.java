package carpetaProjecteBuscaMines;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.lang.Math;


/**
 * Classe on es desenvolupa tota l'activitat del programa.
 * @version 1.0
 * @author arnauicat
 * @since 14-02-2022
 */

public class Jugar2 {

	static Scanner reader = new Scanner(System.in);
	
	/* 
	   He decidit declarar aquesta variable com a global. Quan el jugador vol sortir de 
	   la partida, una variable global ajuda a facilitar les comprovacions pertinents.
	   Si ho hagu�s fet com a par�metre, la quantitat d'operacions hauria sigut m�s gran.   
	*/
	static boolean exit = false;

	
	/**
	 * Metode on es crea el taular i s'inicialitzen totes les caselles amb el valor de Casella.Tapada.
	 * @param files fa referencia al numero de files que tindr� la matriu.
	 * @param columnes fa referencia al numero de columnes que tindr� la matriu. 
	 * @return retura el tauler actualitzat amb el num de columnes i fles correctes i les caselles tapades.
	 * */
	
	public static Casella[][] crearTaulellTapat(int files, int columnes) {
		//Crear taulell que l'usuari podr� veure
		Casella[][] taulellTapat = new Casella[files][columnes];
		
		for (int i = 0; i < files; i++) {
			for (int j = 0; j < columnes; j++) {
				taulellTapat[i][j] = Casella.Tapada;
			}
		}
		return taulellTapat;
	}
	
	/**
	 * M�tode per mostrar la matriu. Ho realitza mitjan�ant una traducci� dels valors de Casella.
	 * @param taulell li enviem el taulell que volem mostrar.
	 * @param files fa referencia al numero de files que te la matriu.
	 * @param columnes fa referencia al numero de columnes que te la matriu.
	 * */
	
	public static void motrar(Casella[][] taulell, int files, int columnes) {
		//Metode per mostrar el qualsevol taulell
		System.out.print("   ");
		for (int i = 0; i < columnes; i++) {
			System.out.print(" "+i+" ");
		}
		System.out.println();
		
		for (int i = 0; i < files; i++) {
			System.out.print(" "+i+" ");
			for (int j = 0; j < columnes; j++) {
				switch(taulell[i][j]) {
					case Tapada:System.out.print(" ? ");
								break;
					case Blanc:System.out.print(" - ");
								break;
					case Mina:	System.out.print(" * ");
								break;
					case Un: 	System.out.print(" "+1+" ");
								break;
					case Dos: 	System.out.print(" "+2+" ");
								break;
					case Tres: 	System.out.print(" "+3+" ");
								break;
					case Quatre:System.out.print(" "+4+" ");
								break;
					case Cinc: 	System.out.print(" "+5+" ");
								break;
					case Sis: 	System.out.print(" "+6+" ");
								break;
					case Set: 	System.out.print(" "+7+" ");
								break;
					case Vuit: 	System.out.print(" "+8+" ");
								break;
				}
			}
			System.out.println();
		}
		System.out.println("\n");
	}
	
	
	/**
	 * M�tode encarregat de crear la segona matriu amb els n�meros i mines col�locades.
	 * Fem servir aquest m�tode com a distribu�dor d'altres metodes que son els que assignaran els numeros i les mines.
	 * @param files fa referencia al numero de files que te la matriu.
	 * @param columnes fa referencia al numero de columnes que te la matriu.
	 * @param mines numero de mines que volem que tingui la matriu
	 * @param posFila posicio de la fila. Junt amb la posColumna, fem servir aquestes posicions per evitar que la primera casella sigui una mina.
	 * @param posColumna posicio de la columna. Junt amb la posFila, fem servir aquestes posicions per evitar que la primera casella sigui una mina.
	 * @return retornem el segon taulell anomenat amb els numeros i mines col�locades.
	 * */
	
	public static Casella[][] crearTaulellMinesNumeros(int files, int columnes, int mines, int posFila, int posColumna) {
		//Metode per crear el taulell de comprovacions
		Casella[][] taulellMinesNumeros = new Casella[files][columnes];
		
		taulellMinesNumeros = posarMines(files, columnes, mines, taulellMinesNumeros, posFila, posColumna);//Retorna taulell amb mines i caselles en blanc
		
		taulellMinesNumeros = posarNumeros(files, columnes, taulellMinesNumeros);
		
		//Retornem un taular amb les mines i numeros posats
		return taulellMinesNumeros;
	}
	
	/**
	 * Metode auxiliar del 'crearTaulellMinesNumeros'. L'utilitzem per col�locar mines al segon taulell.
	 * @param files fa referencia al numero de files que te la matriu.
	 * @param columnes fa referencia al numero de columnes que te la matriu.
	 * @param mines numero de mines que volem que tingui la matriu
	 * @param taulellMinesNumeros taulell al qual li col�locarem totes les mines.
	 * @param posFila posicio de la fila. Junt amb la posColumna, fem servir aquestes posicions per evitar que la primera casella sigui una mina.
	 * @param posColumna posicio de la columna. Junt amb la posFila, fem servir aquestes posicions per evitar que la primera casella sigui una mina.
	 * @return retorna el segon taulell anomenat taulellMinesNumeros amb les mines.
	 * */
	
	
	public static Casella[][] posarMines(int files, int columnes, int mines, Casella[][] taulellMinesNumeros, int posFila, int posColumna) {
		//Posem mines aleatoriament a tot el taulell i segons la quantitat de mines que l'usuari ha volgut
		int randomFila = 0;
		int randomColumna =0;

		//Creem el taulell de les mines i numeros i el rellenem amb caselles en blanc
		for (int i = 0; i < files; i++) {
			for (int j = 0; j < columnes; j++) {
				taulellMinesNumeros[i][j] = Casella.Blanc;
			}
		}
		
		//Posem el numero correcte de mines les mines 
		do {
			randomFila = (int) (Math.random()*files); // Cerquem un numero que del 0 al numero de files (no inclos)
			randomColumna = (int) (Math.random()*columnes); // Cerquem un numero que del 0 al numero de columnes (no inclos)
																		/*Especifiquem que no pot ser la primera posicio que ha posat el jugador*/
			if (!(taulellMinesNumeros[randomFila][randomColumna]==Casella.Mina) && randomFila != posFila && randomColumna != posColumna) { 
				taulellMinesNumeros[randomFila][randomColumna] = Casella.Mina;
				mines--;
			}
			
		} while (mines>0);
		
		return taulellMinesNumeros;
	}
	
	/**
	 * Metode auxiliar del 'crearTaulellMinesNumeros'. L'utilitzem per col�locar numeros al segon taulell.
	 * @param files fa referencia al numero de files que te la matriu.
	 * @param columnes fa referencia al numero de columnes que te la matriu.
	 * @param taulellMinesNumeros parametre on enviem el segon taulell al qual se li acaven d'agregar les mines al metode. 
	 * @return retorna el segon taulell anomenat taulellMinesNumeros amb les mines i els numeros col�locats.
	 * */
	
	public static Casella[][] posarNumeros(int files, int columnes, Casella[][] taulellMinesNumeros) {
		//Asignem els numeros segons la quantiat de mines que estiguin tocant
		Casella [] valors = {Casella.Bandereta, Casella.Un, Casella.Dos, Casella.Tres, Casella.Quatre, Casella.Cinc, Casella.Sis, Casella.Set, Casella.Vuit};
		
		int cont = 0;
		
		for (int i = 0; i < files; i++) {
			for (int j = 0; j < columnes; j++) {
				cont = 0;
				if (taulellMinesNumeros[i][j] == Casella.Blanc) {
					
					if (i-1 >=0 && j-1 >=0 && taulellMinesNumeros [i-1][j-1] == Casella.Mina) cont++;
					if (i-1 >=0 && taulellMinesNumeros [i-1][j] == Casella.Mina) cont++;
					if (i-1 >=0 && j+1 < columnes && taulellMinesNumeros [i-1][j+1] == Casella.Mina) cont++;
					if (j-1 >=0 && taulellMinesNumeros [i][j-1] == Casella.Mina) cont++;
					if (j+1 < columnes && taulellMinesNumeros [i][j+1] == Casella.Mina) cont++;
					if (i+1 < files && j-1 >=0 && taulellMinesNumeros [i+1][j-1] == Casella.Mina) cont++;
					if (i+1 < files && taulellMinesNumeros [i+1][j] == Casella.Mina) cont++;
					if (i+1 < files && j+1 < columnes && taulellMinesNumeros [i+1][j+1] == Casella.Mina) cont++;
					
					if (cont>0) taulellMinesNumeros[i][j] = valors[cont];		
				}
			}
		}
		return taulellMinesNumeros;
	}
	
	/**
	 * Metode on es desenvolupa tota la o les partides del busca mines.
	 * @param opcions ArrayList amb la quantitat de files, columnes i mines que haur� de tenir la nostra matriu.
	 * */
	
	public static void jugarPartida(ArrayList opcions) {
		
		exit = false;
		
					/*///INICIALITZEM DIFERENTS VALORS///*/

		String nomJugador = (String) opcions.get(0); // Inicialitzem nom jugador
		
		ArrayList opcionsEscollides = (ArrayList) opcions.get(1); // Inicialitzem opcions escollides
		
		int files = (int) opcionsEscollides.get(0); // Inicialitzem numero de files
		
		int columnes = (int) opcionsEscollides.get(1); // Inicialitzem numero de columnes
		
		int mines = (int) opcionsEscollides.get(2); // Inicialitzem el numero de mines
		
		////////////////////////////////////////////////////////////////////////////////////////
			
		Casella[][] taulellTapat = crearTaulellTapat(files, columnes); // Inicialitzem taulell tapat
		
		motrar(taulellTapat, files, columnes);
		
		System.out.println("�Recorda, si vols sortir, escriu un 100!\n");
		
		int posFila = demanarPosicio(files, columnes, 1);
		
		int posColumna = 0;
		
		if (exit==false) {
			posColumna = demanarPosicio(files, columnes, 2);
		}
		
		if (exit == false) {
			
			Casella[][] taulellMinesNumeros = crearTaulellMinesNumeros(files, columnes, mines, posFila, posColumna);
			
			actualitzarTaulellTapat2(files, columnes, taulellMinesNumeros, posFila, posColumna, taulellTapat);
			
			motrar(taulellTapat, files, columnes);
			
			////////////////////////////////////////////////////////////////////////////////////////
			
			boolean primeraTirada = true;
			
			boolean estatusPartida = false;
			
			int haAcabat = (files*columnes) - mines;
			
			do {
				
				if (primeraTirada) {
					primeraTirada = false;
					actualitzarTaulellTapat2(files, columnes, taulellMinesNumeros, posFila, posColumna, taulellTapat);
				} else {
					posFila = demanarPosicio(files, columnes, 1);
					if (exit==false) {
						posColumna = demanarPosicio(files, columnes, 2);
						actualitzarTaulellTapat2(files, columnes, taulellMinesNumeros, posFila, posColumna, taulellTapat);
						motrar(taulellTapat, files, columnes);
					}	
				}
				
				if (exit==false) {
					estatusPartida = estatusPartida(taulellTapat, files, columnes);
					
					haAcabat--;
					
					motrar(taulellMinesNumeros, files, columnes);
				}
				
			} while (estatusPartida==false && haAcabat>0 && exit == false);
		
			if(exit==false) {
				if (estatusPartida) System.out.println("HAS PERDUT!!!");
				else System.out.println("HAS GUANYAT!!!");
				System.out.println();
				
				Visualitza.actualitzarJugador(estatusPartida, nomJugador);
			}
		}
		
		System.out.println("Sortint de la partida...\n");
	
	}
	
	/**
	 * Metode on es comprova si dintre de la matriu que veu el jugador ha destapat una Casella.Mina
	 * @param taulellTapat taulell on l'usuari ha anat jugant
	 * @param files fa referencia al numero de files que te la matriu.
	 * @param columnes fa referencia al numero de columnes que te la matriu.
	 * @return Si el resultat es false, es que no ha trobat ninguna. Si el resultat es true, es que si que ha trobat.
	 * */

	public static boolean estatusPartida(Casella[][] taulellTapat, int files, int columnes) {
		//Comprovem si ha destapat alguna mina, si ho ha fet, retornem un false que desactiva el bucle de partida
		for (int i = 0; i <files; i++) {
			for (int j = 0; j < columnes; j++) {
				if (taulellTapat[i][j]==Casella.Mina) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Metode on s'intercanvia la posicio que hem introduit al 'taulellTapat', per la casella que tenim al 'taulellMinesNumeros'.
	 * @param files fa referencia al numero de files que te la matriu.
	 * @param columnes fa referencia al numero de columnes que te la matriu.
	 * @param taulellMinesNumeros Taulell on apareixen les mines i els numeros
	 * @param posFila posicio de fila que ha escollit el jugador
	 * @param posColumna posicio de columna que ha escollit el jugador
	 * @param taulellTapat taulell on ha jugat el jugador
	 * @return retorna el taulell tapat 
	 * */

	public static Casella[][] actualitzarTaulellTapat(int files, int columnes, Casella[][] taulellMinesNumeros, int posFila, int posColumna, Casella[][] taulellTapat) {
		//Canviem la posicio que ens ha donat l'usuari, per la posicio que esta al taulell de comprovacio.
		taulellTapat[posFila][posColumna] = taulellMinesNumeros[posFila][posColumna];
		return taulellTapat;
	}
	
	/**
	 * Metode on es descobreixen les caselles de color blanc en cascada.
	 * @param files fa referencia al numero de files que te la matriu.
	 * @param columnes fa referencia al numero de columnes que te la matriu.
	 * @param taulellMinesNumeros Taulell on apareixen les mines i els numeros
	 * @param i posicio de fila que ha escollit el jugador
	 * @param j posicio de columna que ha escollit el jugador
	 * @param taulellTapat taulell on l'usuari ha anat jugant
	 * @return retorna 0 
	 * */
	
	public static int actualitzarTaulellTapat2(int files, int columnes, Casella[][] taulellMinesNumeros, int i, int j, Casella[][] taulellTapat) {
		
		int caselles = 0;
		
		if (i>=0 && i < files && j >= 0 && j < columnes && taulellTapat[i][j] == Casella.Tapada) {
			
			taulellTapat[i][j] = taulellMinesNumeros[i][j];
			
			if(taulellMinesNumeros[i][j] == Casella.Blanc) {
				
				actualitzarTaulellTapat2(files, columnes, taulellMinesNumeros, i-1, j-1, taulellTapat);
				actualitzarTaulellTapat2(files, columnes, taulellMinesNumeros, i-1, j, taulellTapat); 
				actualitzarTaulellTapat2(files, columnes, taulellMinesNumeros, i-1, j+1, taulellTapat); 
				actualitzarTaulellTapat2(files, columnes, taulellMinesNumeros, i, j+1, taulellTapat);
				actualitzarTaulellTapat2(files, columnes, taulellMinesNumeros, i, j-1, taulellTapat);
				actualitzarTaulellTapat2(files, columnes, taulellMinesNumeros, i+1, j-1, taulellTapat);
				actualitzarTaulellTapat2(files, columnes, taulellMinesNumeros, i+1, j, taulellTapat);
				actualitzarTaulellTapat2(files, columnes, taulellMinesNumeros, i+1, j+1, taulellTapat);	
			}
			
		}
		return 0;
	}
	
	/**
	 * Metode fet servir per demanar la posicio i validarla. 
	 * @param files fa referencia al numero de files que te la matriu.
	 * @param columnes fa referencia al numero de columnes que te la matriu.
	 * @param i parametre que fem servir per demanar les columnes o les files. Si es 1, demanem les files. Si es 2, les columnes
	 * @return retorna el numero introduit per l'usuari previament validat 
	 * */
	
	public static int demanarPosicio(int files, int columnes, int i) {
		
		if (exit==false) {
			
			int posicio = 0;
			int mesures = 0;
			do {
				
				if(i==1) {
					System.out.print("Digam la posicio per a la fila, entre 0 i "+(files-1)+": ");
					posicio = reader.nextInt();
					mesures = files;
					
				} else {
					System.out.print("Digam la posicio per a la columna, entre 0 i "+(columnes-1)+": ");
					posicio = reader.nextInt();
					mesures = columnes;

				} 
				
				if (posicio==100) {
					System.out.print("Per confirmar la sortida torna a apuntar 100: ");
					posicio = reader.nextInt();
					if (posicio==100) {
						exit = true;
						return 0;
					} else posicio = demanarPosicio(files, columnes, i);
				}
				
			} while (posicio>=mesures||posicio<0);
			System.out.println();
			return posicio;
			
		} else System.out.println("Tancant programa...");
		
		return 0;
	}
	
}