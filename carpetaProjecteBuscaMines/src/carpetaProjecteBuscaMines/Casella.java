package carpetaProjecteBuscaMines;

/**
 * Declaraci� dels valors per a les caselles.
 */

//UN COMENTARI

public enum Casella {
	
	/**Color blanc*/
	Blanc, 
	
	/**Opci�  de casella tapada*/
	Tapada, 
	 
	 /**Opci� de bandera*/
	Bandereta, 
	 
	 /**Opci� de mina*/
	Mina, 
	 
	/**Numero 1 */
	Un, 
	 
	/**Numero 2*/
	Dos, 
	 
	/**Numero 3*/
	Tres, 
	 
	/**Numero 4*/
	Quatre, 
	 
	/**Numero 5*/
	Cinc, 
	 
	/**Numero 6*/
	Sis, 
	 
	/**Numero 7*/
	Set, 
	 
	/**Numero 8*/
	Vuit
 //
}
