package carpetaProjecteBuscaMines;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * <h2>Classe Programa</h2> 
 * La classe programa serveix com a distribu�dor dels altres m�todes.
 * @see <a href="http://www.google.com">Google</a>
 * @version 1.0
 * @author arnauicat
 * @since 14-02-2022
 */

public class Programa {
	
	/**
	 * Definici� de l'entrada del teclat
	 */
	
	static Scanner reader = new Scanner(System.in);

	/**
	 * m�todes principal. Aquest m�todes es l'encarregat de cridar a la resta de classes
	 * mitjan�ant un numero que aconseguim amb el metode 'menu()'.
	 * @param args par�metre intern del programa
	 */
	
	public static void main(String[] args) {
		
		int opcio;
		boolean definit = false;
		ArrayList opcions = new ArrayList();
		
		do {
			opcio = menu();
			switch (opcio) {
				case 1: Ajuda.mostrarInfo();
						break;

				case 2: opcions = Opcions.opcions();
						definit = true;
						break;
					
				
				case 3: if (definit) {// Per sortir, has d'apuntar "100" al xat
							Jugar2.jugarPartida(opcions);
							definit = false;
						} else System.out.println("Primer has de definir les opcions!\n");
						break;
				
				case 4: Visualitza.mostrarJugador();
						break;
						
				case 0: System.out.println("Moltes gracies per jugar\n\nAdeu!!!");
						break;
						
				default: System.out.println("Error, opcio incorrecta. Torna-ho a provar...");
			}
			
		} while (opcio != 0);
	}

	/**
	 * Metode el qual imprimeix un menu i valida l'entrada d'un valor enter.
	 * @return Retorna l'opcio del menu que ha seleccionat l'usuari.
	 */
	
	private static int menu() {
		int op = 0;
		boolean valid = false;
		System.out.println("~~~~~~~~~~~~~~~~~~");
		System.out.println("1.- Mostrar Ajuda");
		System.out.println("2.- Opcions");
		System.out.println("3.- Jugar Partida");
		System.out.println("4.- Veure Rankings");
		System.out.println("0.- Sortir");
		System.out.print("\nEscull una opcio: ");
		do {
			try {
				op = reader.nextInt();
				valid = true;
			}
			catch (Exception e) {
				System.out.println("Error, has de posar un numero enter");
				reader.nextLine();
			}
		} while(!valid);
		System.out.println("~~~~~~~~~~~~~~~~~~");
		System.out.println("\n");
		
		return op;
	}

}